#!venv/bin/python
#1111
import os
from flask import Flask, url_for, redirect, render_template, request, abort, json, Response, request, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, current_user
from flask_security.utils import encrypt_password
import flask_admin
from flask_admin.contrib import sqla
from flask_admin import helpers as admin_helpers
from flask_admin import BaseView, expose, AdminIndexView
from flask_admin.form import SecureForm
from flask_admin.actions import action
from os import path, getcwd
from werkzeug.utils import secure_filename
from sqlalchemy.sql import text
from flask_admin import form
from datetime import date, timedelta, datetime
from flask import flash
from PIL import Image
from wtforms import HiddenField, IntegerField, Form, BooleanField, StringField, validators
from wtforms.validators import InputRequired
from flask_admin.helpers import get_redirect_target
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from sqlalchemy import func
from jinja2 import Markup
import time
import shutil
import base64
import sqlite3
import subprocess
import socket
import numpy as np
import cv2
from flask_socketio import SocketIO, emit
import json as js
from flask_admin.contrib.sqla.filters import BaseSQLAFilter, BooleanEqualFilter
from sqlalchemy.ext.hybrid import hybrid_property
import time
import atexit
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.blocking import BlockingScheduler

from flask_babelex import Babel, gettext, lazy_gettext
from wifi import Cell, Scheme
import sys
import multiprocessing
from flask._compat import text_type
from flask.json import JSONEncoder as BaseEncoder
from speaklater import _LazyString
from flask_mobility import Mobility
from flask_mobility.decorators import mobile_template
import subprocess
import requests
import datetime
import netifaces as ni
import iwlist
import csv
import re

gettext = lazy_gettext

class JSONEncoder(BaseEncoder):
    def default(self, o):
        if isinstance(o, _LazyString):
            return text_type(o)

        return BaseEncoder.default(self, o)

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/opt/face_nano/TTS-Demo-a1c484535a80.json'
# Create Flask application
app = Flask(__name__)
Mobility(app)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
babel = Babel(app)
socketio = SocketIO(app)
app.config['file_allowed'] = ['image/png', 'image/jpeg', 'application/octet-stream']
app.config['json_config_file'] = '/home/pi/config.txt'

app.json_encoder = JSONEncoder
basedir = os.path.abspath(os.path.dirname(__file__))
version_path = os.path.join(basedir, 'version.txt')

server_url = ""
boxId = ""

language = 'vi'

@babel.localeselector
def get_locale():
	if language != '':
		return language
	# will return language code (en/es/etc).
	return request.accept_languages.best_match(Config.LANGUAGES.keys())
	
@app.route('/translate', methods=['POST'])
def translate():
        lang = request.get_json()
        globals()['language'] = lang['lang']
        if (os.path.isfile(app.config['json_config_file'])):
            data = {}
            with open(app.config['json_config_file']) as json_file:  
                data = json.load(json_file)

                with open(app.config['json_config_file'], 'w') as outfile:  
                    data['lang'] = lang['lang']
                    json.dump(data, outfile)

        get_locale()
        return ''

def strAndhex2RealStr(strbegin):
    result = b""
    result = bytearray(result)
    i = 4
    while i < len(strbegin): 
        if(strbegin[i] == "\\"):
            result.append(int(strbegin[i+2:i+4], 16))
            i += 4
        else:
            result.append(ord(strbegin[i]))
            i += 1
    b = result.decode('utf8')
    return b
def getWifiList():
    #cell = Cell.all('wlan1')

    # convert map to set
    #setSSID = set(cell)
    #print(setSSID)
    #result = ""
    #for x in setSSID:
    #    x = str(x)
#        temp = x[len('Cell(ssid='):len(x)-1]
    #    result += strAndhex2RealStr(x) + " "
    #return result
    content = iwlist.scan(interface='wlan1')
    cells = iwlist.parse(content)
    print(cells)
    result = ""
    results = []
    for cell in cells:
        if (('essid' in cell) and  cell['essid'] not in results) and (cell['essid'] != "")  and (cell['essid'] != " "):
            results.append(cell['essid']) 
    for x in results:
        result += x + "^"

    return result

def createWifiConfig(SSID, password):
    config_lines = [
        'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev',
        'update_config=1',
        'country=IN',
        '\n',
        'network={',
        '\tssid="{}"'.format(SSID.strip()),
        '\tpsk="{}"'.format(password.strip()),
        #'\tkey_mgmt=WPA-PSK',
        '}'
    ]

    config = '\n'.join(config_lines)
    print(config)

    with open("/etc/wpa_supplicant/wpa_supplicant.conf", "w") as wifi:
        wifi.write(config)

    print("Wifi config added")

def success_handle(output, status=200, mimetype='application/json'):
    return Response(output, status=status, mimetype=mimetype)


def error_handle(error_message, status=500, mimetype='application/json'):
    return Response(json.dumps({"error": {"message": error_message}}), status=status, mimetype=mimetype)


# Define models
roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255))
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    is_unknown = db.Column(db.Boolean(), default=False)
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        if (self.last_name != None):
            return "{0} {1}".format(self.first_name,self.last_name)
        else:
            return self.first_name


# Setup Flask-Security
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


# Create customized model view class
class MyModelView(sqla.ModelView):


    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser'):
            return True

        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                # permission denied
                abort(403)
            else:
                # login
                return redirect(url_for('security.login', next=request.url))


    # can_edit = True
    edit_modal = True
    create_modal = True    
    can_export = True
    can_view_details = True
    details_modal = True

class MyHomeView(AdminIndexView):
    @expose('/')
    def index(self):
        global boxId
        if (os.path.isfile(app.config['json_config_file'])):
            with open(app.config['json_config_file']) as json_file:  
                data = json.load(json_file)
                self._template_args['server_url'] = data['server_url'] if 'server_url' in data else ""
                server_url = self._template_args['server_url']
                self._template_args['ssid'] = data['ssid'] if 'ssid' in data else ""
                self._template_args['serial_number'] = data['serial_number'] if 'serial_number' in data else ""
                boxId = self._template_args['serial_number']
                f = open(version_path, "r")
                v = f.read()
                self._template_args['version'] = v if v else "0.0.1"
        return self.render('admin/setting.html')

@app.template_filter('total_time')
def total_time(entry):
    """
    textual representation of total work time
    """
    total_min = entry.total_min
    if total_min:
        return "%d:%02d" % (total_min // 60, total_min % 60)


@app.template_filter('hhmm')
def hhmm(time):
    """
    textual representation of time in format HH:MM
    """
    return time if time else ''


# Flask views
@app.route('/')
def index():

    return render_template('index.html')

@app.route('/register')
@mobile_template("admin/{mobile/}register.html")
def register(template):
    return render_template(template)


@app.route('/submit', methods=['POST'])
def submit():
    # get name in form data

    id = request.form['id']
    name = request.form['fullname']
    phone = request.form['phone']
    email = request.form['email']
    employee_code = request.form['employee_code']

    ip = request.remote_addr
    print(name)
    print(phone)
    print(email)
    print(ip)

    mac_addr = ""
    for i in range(0,5):
        try:
            print(i)
            output = subprocess.check_output("sudo arp-scan --interface=wlan1  " + ip + " | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}'", shell=True)
            mac_addr = output.decode("utf-8").rstrip("\n")

            break
        except:
            continue
        break
    
    print(mac_addr)

    if (mac_addr != ""):
        url = server_url + "employees" # Set destination URL here
        print(url)
        payload = {'id': id, 'name': name, "phone": phone, "email" : email, "boxId": boxId, "mac": mac_addr, "employee_code": employee_code}     # Set POST fields here

        response = requests.post(url, data=payload)

        print(response.text) #TEXT/HTML
        print(response.status_code, response.reason) #HTTP
        msg = json.loads(response.text)
        #return success_handle(response.text)
        if (response.status_code == 200):
            return render_template("admin/success.html")
        else:
            #return error_handle(response.text)
            return render_template("admin/failed.html", msg=msg["message"])

    return render_template("admin/failed.html")
    #return error_handle(gettext('Cannot register'))    

@app.route('/api', methods=['GET'])
def homepage():
    output = json.dumps({"api": '1.0'})
    return success_handle(output)

@app.route('/api/wifi_setting', methods=['POST'])
def wifi_setting():
    output = json.dumps({"success": True})
    
    # get name in form data
    ssid = request.form['setting_wifi_ssid']
    password = request.form['setting_wifi_password']

    if (os.path.isfile(app.config['json_config_file'])):
        data = {}
        with open(app.config['json_config_file']) as json_file:  
            data = json.load(json_file)

            with open(app.config['json_config_file'], 'w') as outfile:  
                data['ssid'] = ssid
                json.dump(data, outfile)

    createWifiConfig(ssid, password)
    #os.system('sudo bash /etc/ConfigNW.sh -m wifi -s %s  -p %s -e 5' % (ssid,password))
    os.system('sh /home/pi/system.sh --disable-access-point')
    output = json.dumps({"success": True, "message": gettext("Wifi setting done")})
    return success_handle(output)

@app.route('/api/wifi_scaning', methods=['POST'])
def wifi_scaning():
    wifiList = getWifiList()
    output = json.dumps({"success": True, "wifiList":wifiList, "message":gettext("Wifi Scan Done")})
    return success_handle(output)


@app.route('/api/camera_setting', methods=['POST'])
def camera_setting():
    output = json.dumps({"success": True})

    data = {}  

    with open(app.config['json_config_file']) as json_file:  
        data = json.load(json_file)
        data['admin_url'] = basedir
        data['server_url'] = request.form['setting_camera_server_url']
        server_url = data['server_url']
        with open(app.config['json_config_file'], 'w') as outfile:  
            json.dump(data, outfile)
    
    output = json.dumps({"success": True, "message": gettext("Server Setting Done")})
    return success_handle(output)

@app.route('/api/activate_setting', methods=['POST'])
def activate_setting():
    output = json.dumps({"success": True})
    # get name in form data
    key = request.form['setting_activate']

    #myCmd = 'sh /etc/ConfigNW.sh -m direct -s "'+ssid+'" -p "'+password+'"'
    myCmd = 'sudo ans '+ key + ' | grep "get key"'
    result = subprocess.check_output(myCmd, shell=True)
    print("cmd result:", result)

    if (result != b'get key failure\n'):
        if (os.path.isfile(app.config['json_config_file'])):
            data = {}
            with open(app.config['json_config_file']) as json_file:  
                data = json.load(json_file)

                with open(app.config['json_config_file'], 'w') as outfile:  
                    data['serial_number'] = key
                    boxId = key
                    json.dump(data, outfile)
        myCmd = 'sudo reboot -h '
        print(myCmd)
        os.system(myCmd)
        output = json.dumps({"success": True, "message": gettext("Activation done")})
        return success_handle(output)
    else:
        return error_handle(gettext('Cannot activate the device'))

def build_sample_db():
    """
    Populate a small db with some example entries.
    """

    import string
    import random

    print("build_sample_db")

    db.drop_all()
    db.create_all()

    with app.app_context():
        user_role = Role(name='user')
        staff_role = Role(name='staff')
        super_user_role = Role(name='superuser')
        db.session.add(user_role)
        db.session.add(staff_role)
        db.session.add(super_user_role)
        db.session.commit()

        admin_user = user_datastore.create_user(
            first_name='Admin',
            email='admin',
            password=encrypt_password('MQ1234'),
            roles=[super_user_role]
        )

        staff_user = user_datastore.create_user(
            first_name='Staff',
            email='staff',
            password=encrypt_password('MQ1234'),
            roles=[staff_role]
        )

        db.session.commit()


    return

# Create admin
admin = flask_admin.Admin(
    app,
    gettext('MQ WIFI BOX'),
    base_template='my_master.html',
    template_mode='bootstrap3',
    index_view=MyHomeView(menu_icon_type='fa', menu_icon_value='fa-home')
)

app_dir = os.path.realpath(os.path.dirname(__file__))
database_path = os.path.join(app_dir, app.config['DATABASE_FILE'])
if not os.path.exists(database_path):
    build_sample_db()


# define a context processor for merging flask-admin's template context into the
# flask-security views.
@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=url_for
    )

def get_ip_address(ifname):
    # s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # return socket.inet_ntoa(fcntl.ioctl(
    #     s.fileno(),
    #     0x8915,  # SIOCGIFADDR
    #     struct.pack('256s', ifname[:15])
    # )[20:24])
    ip = ""
    try:
       ni.ifaddresses(ifname)
       ip = ni.ifaddresses(ifname)[ni.AF_INET][0]['addr']
    except:
       print("Cannot get ip address:" + ifname)
    return ip

def joinData(path, fileName, moment):
  daydata={}
  filepath=basedir + "/days/" + fileName + ".csv"

  if not os.path.exists(path):
    return
  
  if not os.path.exists(os.path.dirname(filepath)):
    try:
        os.makedirs(os.path.dirname(filepath))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise

  try:
    with open(filepath, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            daydata[row[0]] = list(row)
  except IOError:
    print("File not found")

  with open(path, newline='') as csvfile:
      spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
      for row in spamreader:
          if (row[0] not in daydata.keys()):
            daydata[row[0]]= [row[0], moment]
          else:
            lastTime = datetime.datetime.strptime(daydata[row[0]][-1], '%Y-%m-%d %H:%M:%S')
            thisTime = datetime.datetime.strptime(moment, '%Y-%m-%d %H:%M:%S')
            if (lastTime < thisTime):
              daydata[row[0]].append(moment)

  with open(filepath, 'w', newline='') as csvfile:
      spamwriter = csv.writer(csvfile, delimiter=',',
                              quotechar='|', quoting=csv.QUOTE_MINIMAL)
      for ip in daydata:
        spamwriter.writerow(daydata[ip])

  return filepath

### Move old file
def archive(path):
  filepath=re.sub("days", "archives", path)
  #print("move path = " + filepath)
  ### Init archives dir
  if not os.path.exists(os.path.dirname(filepath)):
    try:
        os.makedirs(os.path.dirname(filepath))
    except OSError as exc: # Guard against race condition
        if exc.errno != errno.EEXIST:
            raise
  ### Move file from days to archives
  os.rename(path, filepath)

def cleanArchives():
    mypath = basedir + "/archives/"
    onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
    for checkFile in onlyfiles:
        fileDate = re.sub(".csv", "", checkFile).split('_')
        fileDate = datetime.date(int(fileDate[0]), int(fileDate[1]), int(fileDate[2]))
        cacheDays = datetime.timedelta(7)
        checkDay = datetime.date.fromtimestamp(time.time()) - cacheDays
        if (fileDate < checkDay):
            os.remove(mypath + checkFile)

def job(text):    

    t = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
    print('{} --- {}'.format(text, t))

    payload = {'boxId': boxId, 'ip_address': get_ip_address('wlan1'), 'local_address': get_ip_address('eth0'), 'time': t}

    ### Merge new data to ./days file
    now = datetime.datetime.now()
    new_file_name = now.strftime("%Y_%m_%d")
    join_file_0 = joinData(new_file_name + ".csv", new_file_name, t)
    join_file_1 = joinData(new_file_name + "_1.csv", new_file_name, t)
    
    ### Scan all files in ./days then send data to server
    mypath = basedir + "/days/"
    onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
    submitFiles = []
    for checkFile in onlyfiles:
        submitFiles.append((checkFile, open(mypath + checkFile, 'rb')))

    new_url = server_url + "sync-data"
    response = requests.post(new_url, files=submitFiles, data=payload)

    ### Read response from server then move old file to ./archives
    resData = js.loads(response.text) #TEXT/HTML
    if (resData['code'] == 200):
        if (len(resData['data']) > 0):
            for resFile in resData['data']:
                fileDate = re.sub(".csv", "", resFile).split('_')
                fileDate = datetime.date(int(fileDate[0]), int(fileDate[1]), int(fileDate[2]))
                today = datetime.date.fromtimestamp(time.time())
                if (fileDate < today):
                    archive(mypath + resFile)

if __name__ == '__main__':

    if (os.path.isfile(app.config['json_config_file'])):
            with open(app.config['json_config_file']) as json_file:  
                data = json.load(json_file)
                globals()['language'] = data['lang'] if 'lang' in data else "vi"

    scheduler = BackgroundScheduler()
    scheduler.add_job(job, 'interval', minutes=1, args=['timechecker'])

    scheduler.add_job(cleanArchives, 'interval', minutes=5)


    scheduler.start()
    os.system('sudo killall sh')
    os.system('nohup sh system_log.sh > /dev/null 2>&1 &')
    os.system('nohup sh system_log1.sh > /dev/null 2>&1  &')
    #print("1")
    if (os.path.isfile(app.config['json_config_file'])):
        with open(app.config['json_config_file']) as json_file:  
            data = json.load(json_file)
            server_url = data['server_url'] if 'server_url' in data else ""
            boxId = data['serial_number'] if 'serial_number' in data else ""

    socketio.run(app, host='0.0.0.0', port=1234, debug=True, use_reloader=False)

