/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
(function ($, AdminLTE) {

  "use strict";

  /**
   * List of all the available skins
   *
   * @type Array
   */
  var my_skins = [
    "skin-blue",
    "skin-black",
    "skin-red",
    "skin-yellow",
    "skin-purple",
    "skin-green",
    "skin-blue-light",
    "skin-black-light",
    "skin-red-light",
    "skin-yellow-light",
    "skin-purple-light",
    "skin-green-light"
  ];

  //Create the new tab
  var tab_pane = $("<div />", {
    "id": "control-sidebar-options-tab",
    "class": "tab-pane"
  });

  //Create the tab button
  var tab_button = $("<li />", {"class": ""})
      .html("<a href='#control-sidebar-options-tab' data-toggle='tab'>"
      + "<i class='fa fa-desktop'></i>"
      + "</a>");

  //Add the tab button to the right sidebar tabs
  $("[href='#control-sidebar-settings-tab']")
      .parent()
      .before(tab_button);

  //Create the menu
  var demo_settings = $("<div />");

  //Layout options
  demo_settings.append(
      "<h4 class='control-sidebar-heading'>"
      + "Layout Options"
      + "</h4>"
        //Fixed layout
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-layout='fixed' class='pull-right'/> "
      + "Fixed layout"
      + "</label>"
      + "<p>Activate the fixed layout. You can't use fixed and boxed layouts together</p>"
      + "</div>"
        //Boxed layout
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-layout='layout-boxed'class='pull-right'/> "
      + "Boxed Layout"
      + "</label>"
      + "<p>Activate the boxed layout</p>"
      + "</div>"
        //Sidebar Toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-layout='sidebar-collapse' class='pull-right'/> "
      + "Toggle Sidebar"
      + "</label>"
      + "<p>Toggle the left sidebar's state (open or collapse)</p>"
      + "</div>"
        //Sidebar mini expand on hover toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-enable='expandOnHover' class='pull-right'/> "
      + "Sidebar Expand on Hover"
      + "</label>"
      + "<p>Let the sidebar mini expand on hover</p>"
      + "</div>"
        //Control Sidebar Toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-controlsidebar='control-sidebar-open' class='pull-right'/> "
      + "Toggle Right Sidebar Slide"
      + "</label>"
      + "<p>Toggle between slide over content and push content effects</p>"
      + "</div>"
        //Control Sidebar Skin Toggle
      + "<div class='form-group'>"
      + "<label class='control-sidebar-subheading'>"
      + "<input type='checkbox' data-sidebarskin='toggle' class='pull-right'/> "
      + "Toggle Right Sidebar Skin"
      + "</label>"
      + "<p>Toggle between dark and light skins for the right sidebar</p>"
      + "</div>"
  );
 
  tab_pane.append(demo_settings);
  $("#control-sidebar-settings-tab").after(tab_pane);

  setup();

  /**
   * Toggles layout classes
   *
   * @param String cls the layout class to toggle
   * @returns void
   */
  function change_layout(cls) {
    $("body").toggleClass(cls);
    AdminLTE.layout.fixSidebar();
    //Fix the problem with right sidebar and layout boxed
    if (cls == "layout-boxed")
      AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
    if ($('body').hasClass('fixed') && cls == 'fixed') {
      AdminLTE.pushMenu.expandOnHover();
      AdminLTE.layout.activate();
    }
    AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
    AdminLTE.controlSidebar._fix($(".control-sidebar"));
  }

  /**
   * Replaces the old skin with the new skin
   * @param String cls the new skin class
   * @returns Boolean false to prevent link's default action
   */
  function change_skin(cls) {
    $.each(my_skins, function (i) {
      $("body").removeClass(my_skins[i]);
    });

    $("body").addClass(cls);
    store('skin', cls);
    return false;
  }

  /**
   * Store a new settings in the browser
   *
   * @param String name Name of the setting
   * @param String val Value of the setting
   * @returns void
   */
  function store(name, val) {
    if (typeof (Storage) !== "undefined") {
      localStorage.setItem(name, val);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
  }

  /**
   * Get a prestored setting
   *
   * @param String name Name of of the setting
   * @returns String The value of the setting | null
   */
  function get(name) {
    if (typeof (Storage) !== "undefined") {
      return localStorage.getItem(name);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
  }

  /**
   * Retrieve default settings and apply them to the template
   *
   * @returns void
   */
  function setup() {
    var tmp = get('skin');
    if (tmp && $.inArray(tmp, my_skins))
      change_skin(tmp);

    //Add the change skin listener
    $("[data-skin]").on('click', function (e) {
      if($(this).hasClass('knob'))
        return;
      e.preventDefault();
      change_skin($(this).data('skin'));
    });

    //Add the layout manager
    $("[data-layout]").on('click', function () {
      change_layout($(this).data('layout'));
    });

    $("[data-controlsidebar]").on('click', function () {
      change_layout($(this).data('controlsidebar'));
      var slide = !AdminLTE.options.controlSidebarOptions.slide;
      AdminLTE.options.controlSidebarOptions.slide = slide;
      if (!slide)
        $('.control-sidebar').removeClass('control-sidebar-open');
    });


    $("[data-enable='expandOnHover']").on('click', function () {
      $(this).attr('disabled', true);
      AdminLTE.pushMenu.expandOnHover();
      if (!$('body').hasClass('sidebar-collapse'))
        $("[data-layout='sidebar-collapse']").click();
    });

    // Reset options
    if ($('body').hasClass('fixed')) {
      $("[data-layout='fixed']").attr('checked', 'checked');
    }
    if ($('body').hasClass('layout-boxed')) {
      $("[data-layout='layout-boxed']").attr('checked', 'checked');
    }
    if ($('body').hasClass('sidebar-collapse')) {
      $("[data-layout='sidebar-collapse']").attr('checked', 'checked');
    }

  }
})(jQuery, $.AdminLTE);




(function ($, AdminLTE) {

  "use strict";

  /**
   * List of all the available skins
   *
   * @type Array
   */
  var my_skins = [
    "skin-blue",
    "skin-black",
    "skin-red",
    "skin-yellow",
    "skin-purple",
    "skin-green",
    "skin-blue-light",
    "skin-black-light",
    "skin-red-light",
    "skin-yellow-light",
    "skin-purple-light",
    "skin-green-light"
  ];

  //Create the new tab
  var skins_pane = $("<div />", {
    "id": "control-sidebar-skins-tab",
    "class": "tab-pane active"
  });

  //Create the tab button
  var skins_button = $("<li />", {"class": "active"})
      .html("<a href='#control-sidebar-skins-tab' data-toggle='tab'>"
      + "<i class='ion-paintbucket'></i>"
      + "</a>");

  //Add the tab button to the right sidebar tabs
  $("[href='#control-sidebar-options-tab']")
      .parent()
      .before(skins_button);

  //Create the menu
  var skins_settings = $("<div />");

  var skins_list = $("<ul />", {"class": 'list-unstyled clearfix'});

  //Dark sidebar skins
  var skin_blue =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-blue' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px; background: #367fa9;'></span><span class='bg-light-blue' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Blue</p>");
  skins_list.append(skin_blue);
  var skin_black =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-black' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div style='box-shadow: 0 0 2px rgba(0,0,0,0.1)' class='clearfix'><span style='display:block; width: 20%; float: left; height: 7px; background: #fefefe;'></span><span style='display:block; width: 80%; float: left; height: 7px; background: #fefefe;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Black</p>");
  skins_list.append(skin_black);
  var skin_purple =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-purple' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-purple-active'></span><span class='bg-purple' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Purple</p>");
  skins_list.append(skin_purple);
  var skin_green =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-green' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-green-active'></span><span class='bg-green' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Green</p>");
  skins_list.append(skin_green);
  var skin_red =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-red' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-red-active'></span><span class='bg-red' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Red</p>");
  skins_list.append(skin_red);
  var skin_yellow =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-yellow' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-yellow-active'></span><span class='bg-yellow' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #222d32;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin'>Yellow</p>");
  skins_list.append(skin_yellow);

  //Light sidebar skins
  var skin_blue_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-blue-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px; background: #367fa9;'></span><span class='bg-light-blue' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Blue Light</p>");
  skins_list.append(skin_blue_light);
  var skin_black_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-black-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div style='box-shadow: 0 0 2px rgba(0,0,0,0.1)' class='clearfix'><span style='display:block; width: 20%; float: left; height: 7px; background: #fefefe;'></span><span style='display:block; width: 80%; float: left; height: 7px; background: #fefefe;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Black Light</p>");
  skins_list.append(skin_black_light);
  var skin_purple_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-purple-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-purple-active'></span><span class='bg-purple' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Purple Light</p>");
  skins_list.append(skin_purple_light);
  var skin_green_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-green-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-green-active'></span><span class='bg-green' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Green Light</p>");
  skins_list.append(skin_green_light);
  var skin_red_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-red-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-red-active'></span><span class='bg-red' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px'>Red Light</p>");
  skins_list.append(skin_red_light);
  var skin_yellow_light =
      $("<li />", {style: "float:left; width: 33.33333%; padding: 5px;"})
          .append("<a href='javascript:void(0);' data-skin='skin-yellow-light' style='display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)' class='clearfix full-opacity-hover'>"
          + "<div><span style='display:block; width: 20%; float: left; height: 7px;' class='bg-yellow-active'></span><span class='bg-yellow' style='display:block; width: 80%; float: left; height: 7px;'></span></div>"
          + "<div><span style='display:block; width: 20%; float: left; height: 20px; background: #f9fafc;'></span><span style='display:block; width: 80%; float: left; height: 20px; background: #f4f5f7;'></span></div>"
          + "</a>"
          + "<p class='text-center no-margin' style='font-size: 12px;'>Yellow Light</p>");
  skins_list.append(skin_yellow_light);

  skins_settings.append("<h4 class='control-sidebar-heading'>Skins</h4>");
  skins_settings.append(skins_list);

  skins_pane.append(skins_settings);
  $("#control-sidebar-options-tab").after(skins_pane);

  setup();


  /**
   * Replaces the old skin with the new skin
   * @param String cls the new skin class
   * @returns Boolean false to prevent link's default action
   */
  function change_skin(cls) {
    $.each(my_skins, function (i) {
      $("body").removeClass(my_skins[i]);
    });

    $("body").addClass(cls);
    store('skin', cls);
    return false;
  }

  /**
   * Store a new settings in the browser
   *
   * @param String name Name of the setting
   * @param String val Value of the setting
   * @returns void
   */
  function store(name, val) {
    if (typeof (Storage) !== "undefined") {
      localStorage.setItem(name, val);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
  }

  /**
   * Get a prestored setting
   *
   * @param String name Name of of the setting
   * @returns String The value of the setting | null
   */
  function get(name) {
    if (typeof (Storage) !== "undefined") {
      return localStorage.getItem(name);
    } else {
      window.alert('Please use a modern browser to properly view this template!');
    }
  }


    /**
   * Retrieve default settings and apply them to the template
   *
   * @returns void
   */
  function setup() {
    var tmp = get('skin');
    if (tmp && $.inArray(tmp, my_skins))
      change_skin(tmp);

    //Add the change skin listener
    $("[data-skin]").on('click', function (e) {
      if($(this).hasClass('knob'))
        return;
      e.preventDefault();
      change_skin($(this).data('skin'));
    });

    //Add the layout manager
    $("[data-layout]").on('click', function () {
      change_layout($(this).data('layout'));
    });

    $("[data-controlsidebar]").on('click', function () {
      change_layout($(this).data('controlsidebar'));
      var slide = !AdminLTE.options.controlSidebarOptions.slide;
      AdminLTE.options.controlSidebarOptions.slide = slide;
      if (!slide)
        $('.control-sidebar').removeClass('control-sidebar-open');
    });

    $("[data-sidebarskin='toggle']").on('click', function () {
      var sidebar = $(".control-sidebar");
      if (sidebar.hasClass("control-sidebar-dark")) {
        sidebar.removeClass("control-sidebar-dark")
        sidebar.addClass("control-sidebar-light")
      } else {
        sidebar.removeClass("control-sidebar-light")
        sidebar.addClass("control-sidebar-dark")
      }
    });

  }

  var train_data = {
      name: "",
      file: null
  };

  var message = null;
  var active_section = null;

  function render(){

    // clear form data

    //$('.form-item input').val('');
    $('.tabs li').removeClass('active');
    $('.tabs li:first').addClass('active');


    active_section = 'train-content';

    $('#'+active_section).show();


  }

  function update(){


    if(message){
        // render message

        $('.message').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message').html('');
    }

    $('#train-content, #recognize-content').hide();
    $('#'+active_section).show();



  }

  function update_wifi(){


    if(message){
        // render message

        $('.message_wifi').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message_wifi').html('');
    }

  }


  function update_direct(){


    if(message){
        // render message

        $('.message_direct').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message_direct').html('');
    }

    $('#train-content, #recognize-content').hide();
    $('#'+active_section).show();



  }

  function update_camera_settings(){


    if(message){
        // render message

        $('.message_camera').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message_camera').html('');
    }

    $('#train-content, #recognize-content').hide();
    $('#'+active_section).show();



  }

  function update_ip_camera_settings(){


    if(message){
        // render message

        $('.message_ip_camera').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message_ip_camera').html('');
    }

    $('#'+active_section).show();



  }

  function update_activate(){

    if(message){
        // render message

        $('.message_activate').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message_activate').html('');
    }

    $('#train-content, #recognize-content').hide();
    $('#'+active_section).show();

  }

  function update_volume(){
  
    if(message){
        // render message
        $('.message_volume').html('<p class="'+_.get(message, 'type')+'">'+_.get(message, 'message')+'</p>');
    }else{
        $('.message_volume').html('');
    }

    $('#train-content, #recognize-content').hide();
    $('#'+active_section).show();

  }

  $('#train #input-file').on('change', function(event){



      //set file object to train_data
      train_data.file = _.get(event, 'target.files[0]', null);


  });


  // listen for name change
  $('#name-field').on('change', function(event){

      train_data.name = _.get(event, 'target.value', '');

  });

  // listen tab item click on

  $('.tabs li').on('click', function(e){

      var $this = $(this);


      active_section = $this.data('section');

      // remove all active class

      $('.tabs li').removeClass('active');

      $this.addClass('active');

      message = null;

      update();



  });



  $('#wifi_save').click(function(event){
      message = null;

      if($('#setting_wifi_ssid').val() && $('#setting_wifi_password').val()){
          // do send data to backend api

          var train_form_data = new FormData();

          train_form_data.append('setting_wifi_ssid', $('#setting_wifi_ssid').val());
          train_form_data.append('setting_wifi_password', $('#setting_wifi_password').val());

          axios.post('/api/wifi_setting', train_form_data).then(function(response){

              message = {type: 'success', message: response.data.message};
              update_wifi();

          }).catch(function(error){
              message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
              update_wifi();
          });

      }else{
          message = {type: "error", message: "SSID and Password are required."}
      }

      update();
      event.preventDefault();
  });

  $('#wifi_scan').click(function(event){
    var train_form_data = new FormData();
    train_form_data.append('status', 'ok');
    axios.post('/api/wifi_scaning', train_form_data).then(function(response){
      var respondStr = response.data.wifiList;
      console.log(respondStr);
      var arrWifi = respondStr.split('^');
      $('#setting_wifi_ssid').empty();
      var arr = [];
      for(var i = 0; i < arrWifi.length; i++){
        arr.push(arrWifi[i].replace(")", ""))
        console.log(arrWifi[i].replace(")", ""));
      }
      var uniqueArr = $.unique(arr);
      for(var i = 0; i < uniqueArr.length; i++){
	if (uniqueArr[i] != "")
        	$('#setting_wifi_ssid').append($("<option></option>")
                            .attr("value",uniqueArr[i])
                            .text(uniqueArr[i])); 
      }
    }).catch(function(error){
      message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
      update_wifi();
    });
    //update();
    event.preventDefault();
  });

  // setup volume
  $('#volume_save').click(function(event){
    message = null;
    if($('#setting_volume').val()){
      var train_form_data = new FormData();

      train_form_data.append('setting_volume_text', $('#setting_volume_text').val());
      train_form_data.append('setting_default_volume_text', $('#setting_default_volume_text').val());
      train_form_data.append('setting_volume', $('#setting_volume').val());

      axios.post('/api/volume_setting', train_form_data).then(function(response){

          message = {type: 'success', message: response.data.message};
          update_volume();

      }).catch(function(error){
          message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
          update_volume();
      });

    }else{
      message = {type: "error", message: "Unknown error."}
    }
    update_volume();
    event.preventDefault();
  });

  $("#turn_speaker_on").change(function() {
    if(this.checked) {
        message = null;
        var train_form_data = new FormData();
        train_form_data.append('turn_speaker_on', 'ok');
        axios.post('/api/turn_speaker_on', train_form_data).then(function(response){
	  //console.log(response);
          message = {type: 'success', message: response.data.message};
          update_volume();

        }).catch(function(error){
          message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
          update_volume();
        });
        update_volume();
        event.preventDefault();
    } else {
      message = null;
      var train_form_data = new FormData();
      train_form_data.append('turn_speaker_off', 'ok');
      axios.post('/api/turn_speaker_off', train_form_data).then(function(response){

        message = {type: 'success', message: response.data.message};
        update_volume();

      }).catch(function(error){
        message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
        update_volume();
      });
      update_volume();
      event.preventDefault();

    }
  });
  
  
  $('#timing_save').click(function(event){
    //alert("scan bluetooth clicked"); 
    var startOff = $("#start_off").val();
    var endOff = $("#end_off").val();

    var train_form_data = new FormData();
    train_form_data.append('startOff', startOff);
    train_form_data.append('endOff', endOff);

    axios.post('/api/timing_seting', train_form_data).then(function(response){
      message = {type: 'success', message: response.data.message};
      update();

    }).catch(function(error){
      message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
      update();
    });
    event.preventDefault();
  });


  $('#direct_save').click(function(event){
      message = null;

      if($('#setting_direct_ssid').val() && $('#setting_direct_password').val()){
          // do send data to backend api

          var train_form_data = new FormData();

          train_form_data.append('setting_direct_ssid', $('#setting_direct_ssid').val());
          train_form_data.append('setting_direct_password', $('#setting_direct_password').val());

          axios.post('/api/direct_network_setting', train_form_data).then(function(response){

              message = {type: 'success', message: response.data.message};
              update_direct();

          }).catch(function(error){
              message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
              update_direct();
          });

      }else{
          message = {type: "error", message: "SSID and Password are required."}
      }

      update_direct();
      event.preventDefault();
  });


  $('#ip_camera_save').click(function(event){
      message = null;

      var train_form_data = new FormData();
      train_form_data.append('setting_camera_rtsp_url_1', $('#setting_camera_rtsp_url_1').val());
      train_form_data.append('setting_camera_rtsp_url_2', $('#setting_camera_rtsp_url_2').val());

      axios.post('/api/ip_camera_setting', train_form_data).then(function(response){

          message = {type: 'success', message: response.data.message};
          update_ip_camera_settings();

      }).catch(function(error){
    console.log(error);
          message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
          update_ip_camera_settings();
      });

      update_ip_camera_settings();
      event.preventDefault();
  });


  $('#camera_save').click(function(event){
      message = null;

      var train_form_data = new FormData();
      train_form_data.append('setting_camera_server_url', $('#setting_camera_server_url').val());
      train_form_data.append('setting_camera_pos_x', $('#setting_camera_pos_x').val());
      train_form_data.append('setting_camera_pos_y', $('#setting_camera_pos_y').val());
      train_form_data.append('setting_camera_pos_width', $('#setting_camera_pos_width').val());
      train_form_data.append('setting_camera_pos_height', $('#setting_camera_pos_height').val());

      axios.post('/api/camera_setting', train_form_data).then(function(response){

          message = {type: 'success', message: response.data.message};
          update_camera_settings();

      }).catch(function(error){
	  console.log(error);
          message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
          update_camera_settings();
      });

      update_camera_settings();
      event.preventDefault();
  });

  $('#activate_save').click(function(event){
      message = null;

      var train_form_data = new FormData();
      train_form_data.append('setting_activate', $('#setting_activate').val());

      axios.post('/api/activate_setting', train_form_data).then(function(response){

          message = {type: 'success', message: response.data.message};
          update_activate();

      }).catch(function(error){
          message = {type: 'error', message: _.get(error, 'response.data.error.message', 'Unknown error.')}
          update_activate();
      });

      update_activate();
      event.preventDefault();
  });

  $('#updateFW').click(function(event){
    console.log("[LHM log] =============> click update FW");
    var train_form_data = new FormData();
    train_form_data.append('version', $('#version').text() || "0.0.1")
    axios.post('/api/update_fw', train_form_data).then(function(res){

    });
    event.preventDefault();
  });

  var getUrlParameter = function getUrlParameter(sParam) {
      var sPageURL = window.location.search.substring(1),
          sURLVariables = sPageURL.split('&'),
          sParameterName,
          i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
          }
      }
  };

  var imgSrc= "/static/img/camera.jpg?v="+ Date.now();
  $(function() {
      $('#camera_view').attr("src", imgSrc);
  });

  function get_timesheet()
  {
      var selcted_date = $(".timesheet-date").val();
      var selected_class  = $("#selected-class").val();
      if (selcted_date && selected_class) {
        window.location = location.origin + location.pathname + "?date=" + selcted_date + "&class=" + selected_class;
      }
  };

  $(function() {
    var date_str = getUrlParameter('date');
    var date;
    if (date_str)
      date = new Date(date_str);
    else
      date = new Date();
    var class_id = getUrlParameter('class');
    if (class_id)
      $("#selected-class").val(class_id)
    else
      $("#selected-class").val(1)
    $('.timesheet-date').datepicker({
        format: 'mm-dd-yyyy',
        "autoclose": true
    })
    .datepicker("setDate",date)
    .on("changeDate", function() {
        get_timesheet();
        return false;
    });;

    $('#selected-class').change(function(){
        get_timesheet();
        return false;
    })
  });

  // render the app;
  render();
  var options1 = { now: startOff, //hh:mm 24 hour format only, defaults to current time 
                  twentyFour: true, //Display 24 hour format, defaults to false 
                  title: 'Time Picker', //The Wickedpicker's title, 
                };
  var options2 = { now: endOff, //hh:mm 24 hour format only, defaults to current time 
                  twentyFour: true, //Display 24 hour format, defaults to false 
                  title: 'Time Picker', //The Wickedpicker's title, 
                };
  //alert(startOff);
  $('#start_off').wickedpicker(options1);
  $('#end_off').wickedpicker(options2);
 
})(jQuery, $.AdminLTE);
